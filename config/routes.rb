Rails.application.routes.draw do
  get 'users/new'
  root             'static_pages#home'
  get 'contattaci'    => 'static_pages#contattaci'
  get 'lavori'   => 'static_pages#INostriLavori'
  get 'dovesiamo' => 'static_pages#Dovesiamo'
  get 'signup' => 'users#new'
end
