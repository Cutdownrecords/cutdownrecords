require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Cut Down Records"
  end

  test "should get help" do
    get :contattaci
    assert_response :success
    assert_select "title", "Contatti | Cut Down Records"
  end

  test "should get lavori" do
    get :lavori
    assert_response :success
    assert_select "title", "I Nostri Lavori | Cut Down Records"
  end

end